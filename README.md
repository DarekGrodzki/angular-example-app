# AngularExampleApp

This project was created to show two different approaches of state management in Angular. 
One is Redux(NgRx) and second is via services. 
These two approaches are mixed in this project only for presentation reasons and should not be mixed in real app.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

