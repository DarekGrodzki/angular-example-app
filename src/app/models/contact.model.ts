export class Contact {
  constructor(
    public name: string,
    public surname: string,
    public email: string,
    public address: string,
  ) {
  }
}
