import { AuthGuard } from './services/auth.guard';
import { AlreadyLoggedGuard } from './services/already-logged.guard';
import { Routes } from '@angular/router';

export const appRoutes: Routes = [
  {
    path: 'login',
    canActivate: [AlreadyLoggedGuard],
    loadChildren: './modules/authorization/authorization.module#AuthorizationModule'
  },
  {
    path: '',
    canActivate: [AuthGuard],
    loadChildren: './modules/main/main.module#MainModule'
  }
];
