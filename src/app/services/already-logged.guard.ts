import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import { map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AlreadyLoggedGuard implements CanActivate {
  constructor(private router: Router, private store: Store<fromApp.AppState>) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.store.select('auth').pipe(
      take(1),
      map(authState => {
        return authState.user;
      }),
      map(user => {
        const isAuth = !!user;
        if (!isAuth) {
          return true;
        }
        this.router.navigate(['/'], {replaceUrl: true});
      })
    );
  }
}
