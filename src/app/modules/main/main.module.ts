import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { ContactItemComponent } from './components/contact-item/contact-item.component';
import { AddContactComponent } from './components/add-contact/add-contact.component';


@NgModule({
  declarations: [DashboardComponent, ContactItemComponent, AddContactComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([{path: '', component: DashboardComponent}]),
    SharedModule
  ]
})
export class MainModule {
}
