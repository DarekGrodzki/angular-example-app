import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthorizationService } from '../../../authorization/services/autorization.service';
import { DashboardService } from '../services/dashboard.service';
import { Contact } from '../../../../models/contact.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  contacts: Contact[];
  subscription: Subscription;
  showAddContact = false;

  constructor(
    private authService: AuthorizationService,
    private dashboardService: DashboardService
  ) {
  }

  ngOnInit() {
    this.subscription = this.dashboardService.contactsChanged
      .subscribe(
        (contacts: Contact[]) => {
          this.contacts = contacts;
        }
      );
    this.contacts = this.dashboardService.getContacts();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public logout() {
    this.authService.logout();
  }

  public addContact() {
    this.showAddContact = true;
  }

  public hideAddContact() {
    this.showAddContact = false;
  }

}
