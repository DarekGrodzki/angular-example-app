import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Contact } from '../../../../models/contact.model';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  contactsChanged = new Subject<Contact[]>();
  private contacts: Contact[] = [
    new Contact(
      'Matt',
      'Damon',
      'matt.damon@gmail.com',
      'California'
    ),
    new Contact(
      'Ben',
      'Affleck',
      'ben.affleck@gmail.com',
      'California'
    ),
  ];

  getContacts() {
    return this.contacts.slice();
  }

  addContact(contact: Contact) {
    this.contacts.push(contact);
    this.contactsChanged.next(this.contacts);
  }

  deleteContact(index: number) {
    this.contacts.splice(index, 1);
    this.contactsChanged.next(this.contacts);
  }
}
