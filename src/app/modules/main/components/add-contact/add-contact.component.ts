import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DashboardService } from '../services/dashboard.service';
import { Contact } from '../../../../models/contact.model';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss']
})
export class AddContactComponent {
  @Output() closeAddContactOutput = new EventEmitter();
  public addContactForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dashboardService: DashboardService
  ) {
    this.addContactForm = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  public closeAddContact() {
    this.closeAddContactOutput.emit();
  }

  public addContact() {
    this.dashboardService.addContact(new Contact(
      this.addContactForm.get('name').value,
      this.addContactForm.get('surname').value,
      this.addContactForm.get('email').value,
      this.addContactForm.get('address').value
    ));
    this.closeAddContact();
  }
}
