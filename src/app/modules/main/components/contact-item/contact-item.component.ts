import { Component, Input } from '@angular/core';
import { Contact } from '../../../../models/contact.model';
import { DashboardService } from '../services/dashboard.service';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss']
})
export class ContactItemComponent {
  @Input() contact: Contact;
  @Input() index: number;

  constructor(private dashboardService: DashboardService) { }

  public deleteContact() {
    this.dashboardService.deleteContact(this.index);
  }
}
