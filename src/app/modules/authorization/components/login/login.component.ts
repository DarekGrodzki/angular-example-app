import { Component } from '@angular/core';
import { AuthorizationService } from '../../services/autorization.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public loginForm: FormGroup;

  constructor(
    private authService: AuthorizationService,
    private formBuilder: FormBuilder
  ) {
    this.loginForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  public login() {
    this.authService.login(
      this.loginForm.get('email').value,
      this.loginForm.get('fullName').value
    ).subscribe();
  }
}
