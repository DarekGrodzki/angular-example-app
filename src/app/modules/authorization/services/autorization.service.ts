import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromApp from '../../../store/app.reducer';
import * as AuthActions from '../store/auth.actions';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  constructor(private router: Router, private http: HttpClient, private store: Store<fromApp.AppState>) {
  }

  public login(email: string, fullName: string): Observable<any> {
    return this.http
      .post<any>(`https://jsonplaceholder.typicode.com/users`, {
        email,
        fullName
      })
      .pipe(
        tap(() => {
          this.store.dispatch(
            new AuthActions.Login({
              email,
              fullName,
              token: '12345'
            })
          );
          this.router.navigate(['/']);
        })
      );
  }

  public logout() {
    this.store.dispatch(new AuthActions.Logout());
    this.router.navigate(['/login']);
  }

}
